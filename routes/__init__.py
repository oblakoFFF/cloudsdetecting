from flask import Flask, render_template, send_file
import fnmatch
import os
from neuralnetwork.params import dir_test
from neuralnetwork.test_submit import get_pred


app = Flask(__name__, template_folder='templates', static_folder='templates')
app.root_path = os.path.abspath(os.path.join(os.path.dirname(app.config.root_path), '.'))


@app.route('/')
def index():
    return render_template('index.html', title='Home')


@app.route('/program')
def program():
    return render_template('program.html',
                           title='Program',
                           testList=fnmatch.filter(os.listdir(dir_test), '*.jpg'))


@app.route('/program/predict/<path:filename>')
def getPred(filename):
    get_pred(filename)
    return render_template('pred.html',
                           title='Predict',
                           filename=filename)



