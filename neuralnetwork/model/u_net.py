from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, concatenate, Conv2D, MaxPooling2D, UpSampling2D

from neuralnetwork.model.losses import bce_dice_loss, dice_coeff


def model_128(input_shape=(128, 128, 3), num_classes=1):
    
    inputs = Input(shape=input_shape)
    # 128
    
    down1 = Conv2D(64, (3, 3), activation='relu', padding='same')(inputs)
    down1 = Conv2D(64, (3, 3), activation='relu', padding='same')(down1)
    down1_pool = MaxPooling2D(pool_size=(2, 2))(down1)
    # 64
    
    down2 = Conv2D(128, (3, 3), activation='relu', padding='same')(down1_pool)
    down2 = Conv2D(128, (3, 3), activation='relu', padding='same')(down2)
    down2_pool = MaxPooling2D(pool_size=(2, 2))(down2)
    # 32
    
    down3 = Conv2D(256, (3, 3), activation='relu', padding='same')(down2_pool)
    down3 = Conv2D(256, (3, 3), activation='relu', padding='same')(down3)
    down3_pool = MaxPooling2D(pool_size=(2, 2))(down3)
    # 16
    
    down4 = Conv2D(512, (3, 3), activation='relu', padding='same')(down3_pool)
    down4 = Conv2D(512, (3, 3), activation='relu', padding='same')(down4)
    down4_pool = MaxPooling2D(pool_size=(2, 2))(down4)
    # 8
    
    center = Conv2D(1024, (3, 3), activation='relu', padding='same')(down4_pool)
    center = Conv2D(1024, (3, 3), activation='relu', padding='same')(center)
    # center
    
    up4 = UpSampling2D((2, 2))(center)
    up4 = concatenate([down4, up4], axis=3)
    up4 = Conv2D(512, (3, 3), activation='relu', padding='same')(up4)
    up4 = Conv2D(512, (3, 3), activation='relu', padding='same')(up4)
    up4 = Conv2D(512, (3, 3), activation='relu', padding='same')(up4)
    # 16
    
    up3 = UpSampling2D((2, 2))(up4)
    up3 = concatenate([down3, up3], axis=3)
    up3 = Conv2D(256, (3, 3), activation='relu', padding='same')(up3)
    up3 = Conv2D(256, (3, 3), activation='relu', padding='same')(up3)
    up3 = Conv2D(256, (3, 3), activation='relu', padding='same')(up3)
    # 32
    
    up2 = UpSampling2D((2, 2))(up3)
    up2 = concatenate([down2, up2], axis=3)
    up2 = Conv2D(128, (3, 3), activation='relu', padding='same')(up2)
    up2 = Conv2D(128, (3, 3), activation='relu', padding='same')(up2)
    up2 = Conv2D(128, (3, 3), activation='relu', padding='same')(up2)
    # 64
    
    up1 = UpSampling2D((2, 2))(up2)
    up1 = concatenate([down1, up1], axis=3)
    up1 = Conv2D(64, (3, 3), activation='relu', padding='same')(up1)
    up1 = Conv2D(64, (3, 3), activation='relu', padding='same')(up1)
    up1 = Conv2D(64, (3, 3), activation='relu', padding='same')(up1)
    # 128
    
    classify = Conv2D(num_classes, (1, 1), activation='sigmoid')(up1)
    
    model = Model(inputs=inputs, outputs=classify)
    
    model.compile(loss=bce_dice_loss, metrics=[dice_coeff])
    
    return model
