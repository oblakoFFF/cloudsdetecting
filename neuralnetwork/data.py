import neuralnetwork.params as params

dir_test = params.dir_test
input_size = params.input_size
original_width = params.original_width
original_height = params.original_height


def cropper_generator(img):
    x_step = round(original_width / input_size)
    y_step = round(original_height / input_size)
    for x_count in range(x_step):
        for y_count in range(y_step):
            x_start, x_end = input_size * x_count, input_size * (x_count+1)
            y_start, y_end = input_size * y_count, input_size * (y_count+1)
            img_crop = img[y_start: y_end,
                           x_start: x_end]
            yield img_crop
