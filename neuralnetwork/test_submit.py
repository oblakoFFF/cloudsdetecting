import os
import cv2
import numpy as np
import fnmatch

import neuralnetwork.model.u_net as model
import neuralnetwork.params as params

input_size = params.input_size
original_width = params.original_width
original_height = params.original_height
model = model.model_128()
dir_pred = params.dir_pred
dir_test = params.dir_test

from neuralnetwork.data import cropper_generator

print('-' * 30)
print('Loading saved weights...')
print('-' * 30)

model.load_weights('weight.h5')

print('-' * 30)
print('Predicting masks on test data...')
print('-' * 30)

files = fnmatch.filter(os.listdir(dir_test), '*.jpg')
ids_test_imgs = []
test_target_imgs = []


def get_pred(file):
    print('Load ', str(dir_test + '/{}'.format(file)))
    div_width = round(original_width / input_size)
    div_height = round(original_height / input_size)

    image = cv2.imread(dir_test + '/{}'.format(file))
    image = cv2.resize(image, (div_width * input_size, div_height * input_size))

    x_batch = []
    for img in cropper_generator(image):
        x_batch.append(img)

    x_batch = np.array(x_batch, np.float32) / 255

    preds = model.predict_on_batch(x_batch)
    preds = np.squeeze(preds, axis=3)

    line = []
    image = []
    for i, pred in enumerate(preds):
        if i % div_height:
            line = np.concatenate((line, pred), axis=0)
        else:
            image = np.concatenate((image, line), axis=1) if len(image) else line
            line = pred

    image = np.concatenate((image, line), axis=1)

    if not os.path.exists(dir_pred):
        os.mkdir(dir_pred)

    cv2.imwrite(os.path.join(dir_pred, file), (image[:, :] * 255.).astype(np.uint8))

